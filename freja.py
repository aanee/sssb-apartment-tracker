#!/bin/python

from dryscrape import Session
from time import sleep
from bs4 import BeautifulSoup
from subprocess import call

fileName = "frejaResultat"

# url = "https://www.sssb.se/soka-bostad/lediga-bostader/lediga-lagenheter-lista/?paginationantal=all" ## Hela SSSB
url = "https://www.sssb.se/soka-bostad/lediga-bostader/lediga-lagenheter-lista/?omraden=Freja&objektTyper=&hyraMax=&actionId=?paginationantal=all" ## Freja

while True:
    session = Session()
    session.visit(url)
    response = session.body()
    # tag = BeautifulSoup(response, "html.parser").find("div", class_="f2-widget Objektlista Lagenheter")
    tag = BeautifulSoup(response, "html.parser").findAll("div" , class_="media")
    #print("tag:", tag)
    if tag is not None:
        break
    print("Retrying")
    sleep(10)

#print("tag.contents:", tag[0].contents)
# dump = open("htmlDump", "w")
# dump.write(str(tag.contents[1]))
# print("\nNÄSTA\n")
#print("tag.contents[1]:", tag.contents[1])

resultList = []
for thing in tag:
    print(thing)
    #print(thing.contents[1].contents[0])
    # print(thing.contents[3].contents[0])
    # print(thing.contents[5].contents[0])
    # print("Yta:", thing.contents[9].contents[2])
    # print("Hyra:", thing.contents[11].contents[1], "\n")

    resultList.append(str(thing.contents[1].contents[0])
                      + "\n" + str(thing.contents[3].contents[0])
                      + "\n" + str(thing.contents[5].contents[0])
                      + "\nYta: " + str(thing.contents[9].contents[2])
                      + "\nHyra: " + str(thing.contents[11].contents[1]))

print("Resultat:", len(resultList), resultList)

if len(resultList):
    writeFile = None
    readFile = None
    try:
        readFile = open(fileName, "r")
    except IOError:
        print("Shit happened...")

    if readFile is not None:
        newResult = []
        oldResultList = readFile.read().split("###")
        readFile.close()
        # print("Old:", len(oldResultList), oldResultList) # TEST
        indexOld = 0
        old = False
        for thing in resultList:
            for oldThing in oldResultList:
                if thing == oldThing:
                    old = True

            if old is not True:
                newResult.append(thing)

        # print("New:", len(newResult), newResult) # TEST
        writeFile = open(fileName, "w")
        writeFile.write("###".join(resultList))
        writeFile.close()
        ###  Meddela användaren ###
        printString = "\n\n".join(newResult)
        call(["notify-send", printString, "-to", 36000])

    else:
        writeFile = open(fileName, "w")
        writeFile.write("###".join(resultList))
        ### Meddela användaren ###
        printString = "\n\n".join(resultList)
        call(["notify-send", printString, "-to", 36000])
else:
    print("Inga resultat")
